-- Function: public.f_fuzzy3(text[], text[], integer, double precision)

-- DROP FUNCTION public.f_fuzzy3(text[], text[], integer, double precision);

CREATE OR REPLACE FUNCTION public.f_fuzzy3(
    p_tabelas text[],
    p_colunas text[],
    p_ncluster integer,
    p_m double precision)
  RETURNS text AS
$BODY$
DECLARE
	V_Dados 			float[][];
	V_Cluster			float[][];
	V_Pertinencia			float[][];
	V_PertinenciaAux		float[][];
	V_MatDistancia			float[][];
	V_Denominadores			float[];
	V_ArrayTotalDistancia		text;
	V_ArrayParcialDistancia		text;
	V_ArrayPertinencia		text;	
	V_NVariaveis			integer;
	V_Expoente			float;
	V_Maior				float;
	V_Menor				float;
	V_NElementos			integer;
	V_ArrayCentroide		text;
	V_ArrayLocalCentroide		float;
	V_ArrayTotalDados		text;
	V_ArrayParcialDados		text;
	V_ArrayTotalCentroides		text;
	V_ArrayParcialCentroides	text default '';
	V_ArrayTotalPertinencia		text;
	V_ArrayParcialPertinencia	text default '';
	V_Numerador			float default 0;
	V_Denominador			float default 0;
	V_ValorPertinencia		float default 0;
	V_P				float default 0;
	V_PAnt				float default 0;
	V_Parada			integer default 0;
	V_Medida			integer;
	V_Valor				float;
	V_RegGid			record;
	Cursor_organizagid		refcursor;
	i				integer;
	v_nelementosvetor		integer;
	v_vetormediana			float[];
	V_Mediana			float;
	V_Amplitude			float;
	V_MPE				float;
	V_FPI				float;
	V_TREE				refcursor;
	V_SQLTREE			text;
	V_RECTREE			record;
	V_MENORFPI 			FLOAT DEFAULT 1;
	v_MENORMPE                      FLOAT DEFAULT 1;
	v_vetorsaida			float[][];
	v_c1				text;
	v_c2				text;
	valoresfinaiscentroide          text;
	v_sql				text;
	retorno				text;
	V_MENORFPIAUX 			FLOAT;
	v_MENORMPEAUX                   FLOAT;
	v_c2aux				text;
BEGIN

	V_NVariaveis := array_upper(P_Tabelas, 1);

----------------------------------- REALIZA A LEITURA DOS DADOS -----------------------------------------
        V_ArrayTotalDados := '{';
	FOR i IN 1..V_NVariaveis LOOP
		EXECUTE 'SELECT count(*) FROM '||P_Tabelas[i] INTO V_nelementosvetor;
		--raise notice 'Contagem: %', v_nelementosvetor;
		EXECUTE 'SELECT Max(cast('||P_Colunas[i]||' as float))- Min(cast('||P_colunas[i]||' as float)) FROM '||P_Tabelas[i] INTO V_Amplitude;
--		raise notice 'amplitude: %', v_amplitude;
		v_vetorMediana := '{}';
		EXECUTE 'SELECT ARRAY (SELECT '||P_Colunas[i]||' FROM '||P_Tabelas[i]||' order by '||P_Colunas[i]||')' INTO V_VetorMediana;
--		raise notice 'amplitude: %', v_vetormediana;
		if MOD(V_nelementos,2) = 0 then
			v_mediana = (v_vetorMediana[v_nelementosvetor/2]+ v_vetorMediana[(v_nelementosvetor/2)+1])/2;
			raise notice 'ELEMENTOPAR: %', v_nelementosvetor/2;
		else
			v_mediana = v_vetorMediana[(ROUND(v_nelementosvetor/2))];
--			raise notice 'ELEMENTOIMPAR: %', (ROUND(v_nelementosvetor/2))+1;
		end if;
--		raise notice 'mediana: %', v_mediana;
--		EXECUTE 'SELECT ARRAY (SELECT (cast('||P_Colunas[i]||' as float)- ROUND('||V_Mediana||',2))/'||V_Amplitude||' FROM '||P_Tabelas[i]||' order by gid)' INTO V_ArrayParcialDados;
		EXECUTE 'SELECT ARRAY (SELECT cast('||P_Colunas[i]||' as float) FROM '||P_Tabelas[i]||')' INTO V_ArrayParcialDados;
--		raise notice 'Matriz: %', V_ArrayParcialDados;
		V_ArrayTotalDados := V_ArrayTotalDados || V_ArrayParcialDados;
	END LOOP;
	V_ArrayTotalDados := V_ArrayTotalDados||'}';
	SELECT REPLACE (V_ArrayTotalDados, '}{','},{') into V_ArrayTotalDados;
	V_Dados := V_ArrayTotalDados;
	--raise notice 'Matriz: %', V_Dados;
---------------------------------------------------------------------------------------------------------------------

-------------------------------------  INICIALIZA OS CENTRÓIDES -----------------------------------------------------
/*
	V_ArrayTotalCentroides	:= '{';
	FOR i IN 1..P_NCluster LOOP
	        V_ArrayParcialCentroides := '';
		V_ArrayParcialCentroides := V_ArrayParcialCentroides|| '{';
		FOR j in 1..array_upper(P_Colunas, 1) LOOP
		    EXECUTE 'select max('||P_Colunas[j]||') from '||P_Tabelas[j] INTO V_MAIOR;
		    EXECUTE 'select min('||P_Colunas[j]||') from '||P_Tabelas[j] INTO V_MENOR;
		    EXECUTE 'SELECT '||V_Menor||' + round(CAST (random()*('||V_Maior||' - '||V_Menor||') AS NUMERIC),2)' INTO V_ArrayLocalCentroide;
		    V_ArrayParcialCentroides := V_ArrayParcialCentroides||V_ArrayLocalCentroide||', ';
		END LOOP;
		V_ArrayParcialCentroides := V_ArrayParcialCentroides|| '}';
		V_ArrayTotalCentroides := V_ArrayTotalCentroides || V_ArrayParcialCentroides;
	END LOOP;
	SELECT REPLACE (V_ArrayTotalCentroides, ', }','}') INTO V_ArrayTotalCentroides;
	SELECT REPLACE (V_ArrayTotalCentroides, '}{','},{') INTO V_ArrayTotalCentroides;
	V_ArrayTotalCentroides := V_ArrayTotalCentroides ||'}';
	V_Cluster := V_ArrayTotalCentroides;
	-- iniciando para os testes --
	--V_Cluster := '{{0.1, 2},{4.65, 2}}';
	raise notice 'centroid:%', v_cluster;
--------------------------------------------------------------------------------------------------------------------
*/

------------------------------------------    INICIALIZA A PERTINÊNCIA ---------------------------------------------
	V_ArrayTotalPertinencia := '{';
	EXECUTE 'SELECT COUNT(*) FROM '||P_Tabelas[1] INTO V_NELEMENTOS;
	FOR I IN 1..V_NELEMENTOS LOOP
		V_ArrayParcialPertinencia := '';
		V_ArrayParcialPertinencia := V_ArrayParcialPertinencia || '{';
		FOR J IN 1..p_NCLUSTER LOOP
		        IF J = 1 THEN
			     V_ArrayParcialPertinencia := V_ArrayParcialPertinencia || '1, ';
			ELSE
			     V_ArrayParcialPertinencia := V_ArrayParcialPertinencia || '0, ';
			END IF;
		END LOOP;
		V_ArrayParcialPertinencia := V_ArrayParcialPertinencia || '}';
		V_ArrayTotalPertinencia := V_ArrayTotalPertinencia || V_ArrayParcialPertinencia;
	END LOOP;
	SELECT REPLACE (V_ArrayTotalPertinencia, ', }','}') INTO V_ArrayTotalPertinencia;
	SELECT REPLACE (V_ArrayTotalPertinencia, '}{','},{') INTO V_ArrayTotalPertinencia;
	V_ArrayTotalPertinencia := V_ArrayTotalPertinencia || '}';
	V_Pertinencia := V_ArrayTotalPertinencia;
	--raise notice 'Pertinência: %', V_Pertinencia;
--------------------------------------------------------------------------------------------------------------------

--LOOP
  V_SQLTREE := 'SELECT * FROM sdum.teste ORDER BY pon_codigo';
  OPEN V_TREE FOR EXECUTE V_SQLTREE;
     LOOP FETCH V_TREE INTO V_RECTREE;
      EXIT WHEN NOT FOUND;
	    V_CLUSTER := V_RECTREE.CENTROID;
           -- RAISE NOTICE 'array2: %',v_cluster;
 --       end if;
 --    end loop;
--      close v_tree2;
 --    end loop;


-----------------------------  Inicia o cálculo da Pertinência -----------------------------------------------------
--raise notice '------------------ % ------------------------', V_Pertinencia;
        V_ArrayPertinencia := '{';
	FOR I IN 1..V_NELEMENTOS LOOP  -- REPRESENTA CADA LINHA DE DADOS	
		FOR J IN 1..P_NCLUSTER LOOP  -- REPRESENTA O NÚMERO DE CENTROIDES
			V_NUMERADOR   := 0;
			V_DENOMINADOR := 0;
			V_VALORPERTINENCIA := 0;
			FOR K IN 1..V_NVARIAVEIS LOOP -- REPRESENTA O NÚMERO DE VARIÁVEIS
			        if V_Dados[k][i] <>  V_Cluster[j][k] then
			           V_Numerador := V_Numerador + POWER((V_Dados[k][i] -  V_Cluster[j][k]),2);
			        End if;
			        --RAISE NOTICE 'array2: -------------passei--------------------';
			END LOOP;
			FOR P IN 1..P_NCLUSTER LOOP -- REPRESENTA OS CLUSTERS QUE DEVERÃO SER PERCORRIDOS
				V_DENOMINADOR := 0;
				FOR K IN 1..V_NVARIAVEIS LOOP
				    if V_Dados[k][i] <>  V_Cluster[P][k] then
				     V_DENOMINADOR := V_DENOMINADOR + POWER((V_Dados[k][i] -  V_Cluster[P][k]),2);
				    end if;
				    --RAISE NOTICE 'array4: -------------passei--------------------';
				    --RAISE NOTICE 'numerador:% denominador:%:', v_numerador, V_Denominador;
				END LOOP;
				V_DENOMINADORES[P]:= V_DENOMINADOR;
				--RAISE NOTICE 'Denominador:%:', V_Denominador;
			END LOOP;	
				V_ValorPertinencia := 0;
				V_Expoente := (1/(p_m-1));
				

				FOR K IN 1..P_NCluster LOOP   -- mudei era V_NElementos
				--RAISE NOTICE 'numerador:% denominador:%:', v_numerador, V_Denominadores[k];
				       if v_denominadores[k] = 0 then
				          v_denominadores[k] := 0.0000000001;
				       end if;
				       if v_numerador = 0 then
				          v_numerador := 0.000000001;
				       end if;
					V_ValorPertinencia := V_ValorPertinencia + power((sqrt(V_Numerador)/sqrt(V_Denominadores[k])), V_Expoente);
				END LOOP;
				V_ValorPertinencia := POWER(V_ValorPertinencia, (-1));
				--raise notice 'Pertinencia: %',V_ValorPertinencia;
				V_ArrayPertinencia := V_ArrayPertinencia ||V_ValorPertinencia||', '	;	
		END LOOP;
	   V_ArrayPertinencia := V_ArrayPertinencia||'}{';
	END LOOP;
        V_ArrayPertinencia := V_ArrayPertinencia||'}';
	SELECT REPLACE (V_ArrayPertinencia, ', }','}') INTO V_ArrayPertinencia;
	SELECT REPLACE (V_ArrayPertinencia, '}{','},{') INTO V_ArrayPertinencia;
	SELECT REPLACE (V_ArrayPertinencia, ',{}','}') INTO V_ArrayPertinencia;  
	V_Pertinencia := '{'||V_ArrayPertinencia;    
	  
-------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------   CÁLCULO DO CENTRÓIDE ----------------------------------------------------------------------

           SELECT F_MPEindex(v_pertinencia) into  V_MPE;
           SELECT F_FPIindex(v_pertinencia) into  V_FPI;
           --raise notice 'C1:% - MPE:% - FPI:%',V_RECTREE.GID, V_MPE, V_FPI;
           --insert into tb_valores_mpe_fpi values (V_RECTREE.GID, V_MPE, V_FPI);
           IF V_MPE <= V_MENORMPE THEN
              V_MENORMPE = V_MPE;
              v_vetorsaida := v_pertinencia;
              v_c1 := V_RECTREE.pon_codigo;
           END IF;
           IF V_FPI <= V_MENORFPI THEN
              V_MENORFPI = V_FPI;
              --v_vetorsaida := v_pertinencia;
              v_c2 := V_RECTREE.pon_codigo;
           END IF;
     end loop;
----------------------------------------------------------------------------------------------------------------------------------
  Raise notice '-------------------RESULTADO FINAL--------------------------';
  raise notice 'FPI:% --- MPE: % --- Tree:%', V_MENORFPI, V_MENORMPE, V_c2;
  V_MENORFPIAUX =  V_MENORFPI;
  V_MENORMPEAUX = V_MENORMPE;
  V_C2AUX = V_c2;
  --FOR I IN 1..array_upper(v_vetorsaida,1) loop
      --raise notice '%|%|%|%',v_vetorsaida[i][1], v_vetorsaida[i][2], v_vetorsaida[i][3], v_vetorsaida[i][4];
  --end loop;
 close V_TREE;
  v_sql := 'select centroid from sdum.teste where pon_codigo = '''||v_c2||'''' ;
  execute v_sql into valoresfinaiscentroide;
  retorno := V_MENORFPIAUX||', '||V_MENORMPEAUX||', '||V_c2aux||', '||valoresfinaiscentroide;
  

  Raise notice 'Centroid values: %', valoresfinaiscentroide;
  --execute 'drop table sdum.teste';
  return retorno;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_fuzzy3(text[], text[], integer, double precision)
  OWNER TO postgres;
