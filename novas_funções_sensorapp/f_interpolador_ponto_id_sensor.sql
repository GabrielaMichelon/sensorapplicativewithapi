-- Function: public.f_interpolador_ponto_id_sensor(integer, integer, double precision, integer, double precision)

-- DROP FUNCTION public.f_interpolador_ponto_id_sensor(integer, integer, double precision, integer, double precision);

CREATE OR REPLACE FUNCTION public.f_interpolador_ponto_id_sensor(
    p_cod_amostra integer,
    p_cod_grade integer,
    p_expoente double precision,
    p_elementos integer,
    p_raio double precision)
  RETURNS boolean AS
$BODY$
DECLARE
    Cur_Amostras   	 refcursor;
	V_QTDEPONTOS	 text;
	V_QUERY_SELECT 	 text;
    V_Pontos   		 GEOMETRY;
    V_Distancia 	 FLOAT;
    V_Medida    	 FLOAT;
    V_Soma      	 FLOAT:=0;
    V_Numerador		 FLOAT:=0;
    V_Denominador	 FLOAT:=0;
    V_Estimado		 FLOAT:=0;
	V_Descricao		 text;
	V_min_pon_codigo integer;
	V_max_pon_codigo integer;
	V_min_pix_codigo integer;
	V_max_pix_codigo integer;
	V_descricao_semespaco text;
    i 			 	 integer:=0;
	qtdeponto	 	 integer :=0;
   
BEGIN
  -- os parâmetros de entrada da função são ($1 -> código da amostra a ser interpolada tb_amostra, 
						--$2 -> código da grade que armazena os pixels tb_gradeamostral, 
						--$3 -> expoente da interpolacao, 
						--$4 -> numero de elementos (vizinhos), 
						--$5 -> raio (em metros), 
	
	EXECUTE 'select amo_descricao from sdum.tb_amostra where amo_codigo = '||p_cod_amostra into V_Descricao;
	EXECUTE 'select replace('''||V_Descricao||''', '' '', ''_'')' into V_descricao_semespaco;
	EXECUTE 'DROP TABLE IF EXISTS sdum.tb_'||V_descricao_semespaco||'_sensor';
	EXECUTE 'create table sdum.tb_'||V_descricao_semespaco||'_sensor (pon_codigo, the_geom, pix_valor) as select a.pon_codigo, a.the_geom, 0.00 from sdum.tb_pontoamostral a where a.pon_gracodigo = '||p_cod_grade;
	EXECUTE 'select min(pon_codigo) from sdum.tb_pontoamostral where pon_gracodigo = '||p_cod_grade into V_min_pon_codigo;
	EXECUTE 'select max(pon_codigo) from sdum.tb_pontoamostral where pon_gracodigo = '||p_cod_grade into V_max_pon_codigo;
	EXECUTE 'select min(pix_codigo) from sdum.tb_pixelamostra where pix_amocodigo = '||p_cod_amostra into V_min_pix_codigo;
	EXECUTE 'select max(pix_codigo) from sdum.tb_pixelamostra where pix_amocodigo = '||p_cod_amostra into V_max_pix_codigo;

		
	WHILE V_min_pon_codigo <= V_max_pon_codigo LOOP	
						
		V_QUERY_SELECT := 'SELECT a.the_geom, st_distance(st_centroid(b.the_geom), a.the_geom) as distancia,  a.pix_valor as medida
							FROM sdum.tb_pixelamostra a, sdum.tb_pontoamostral b WHERE a.pix_amocodigo = '||p_cod_amostra||' and b.pon_gracodigo = '||p_cod_grade||' and b.pon_codigo = '||V_min_pon_codigo;
        IF P_Elementos > 0 THEN
			V_QUERY_SELECT := V_QUERY_SELECT ||' order by distancia LIMIT '||P_Elementos ;
		ELSE
			V_QUERY_SELECT := V_QUERY_SELECT ||' and distancia < '||P_Raio;
		END IF;
        raise notice 'sql: %',v_query_select;

	    OPEN Cur_Amostras FOR EXECUTE V_QUERY_SELECT;
	
		LOOP FETCH Cur_Amostras INTO V_Pontos, V_Distancia, V_Medida;
		EXIT WHEN NOT FOUND;
		   
		   		   
           V_Numerador := V_Numerador + (V_Medida/(V_Distancia^P_Expoente));
           V_Denominador := V_Denominador + (1/(V_Distancia^P_Expoente));
		   i:= i+1;
        END LOOP;

		IF i > 0 THEN
			V_Soma := V_Numerador/V_Denominador;
			EXECUTE 'UPDATE sdum.tb_'||V_descricao_semespaco||'_sensor set pix_valor = '||V_soma||' WHERE pon_codigo = '||V_min_pon_codigo;
	   ELSE
          EXECUTE 'UPDATE sdum.tb_'||V_descricao_semespaco||'_sensor set pix_valor = 0 WHERE pon_codigo = '||V_min_pon_codigo;
       END IF;
	
	   CLOSE Cur_Amostras;
	    V_min_pix_codigo = V_min_pix_codigo + 1;
		V_min_pon_codigo := V_min_pon_codigo + 1;
		i := 0;
	END LOOP;
	
    RETURN TRUE;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_interpolador_ponto_id_sensor(integer, integer, double precision, integer, double precision)
  OWNER TO postgres;
