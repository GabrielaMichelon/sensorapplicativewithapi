-- Function: public.f_inserecombinacao(integer, integer, integer, text[], text, integer)

-- DROP FUNCTION public.f_inserecombinacao(integer, integer, integer, text[], text, integer);

CREATE OR REPLACE FUNCTION public.f_inserecombinacao(
    p_sensores integer,
    p_elementos integer,
    p_limite integer,
    p_tabelaentrada text[],
    p_tabelasaida text,
    p_qtdepontos integer)
  RETURNS text AS
$BODY$
declare
  v_combinacoes integer :=0;
  v_diferenca integer :=0;
  v_sorteado numeric;
  v_limite integer := p_limite;
  v_temporario integer[] := '{0}';
  v_matriz numeric[] := '{0}';
  v_flag integer;
  n integer;
  posicao integer;
  v_sql text;
  v_sql2 text;
  v_sql3 text;
  v_maxcod integer;
  v_cursor refcursor;
  v_dado record;  
  v_textomatriz text;
  v_sql4 text;
  v_numeroElementos integer :=0;
  v_numeropontostabelaentrada integer;
  v_numericzero numeric;
  v_retorno text;
begin

  execute 'drop table if exists sdum.teste';
  v_sql := 'select max(pon_codigo) from '||p_tabelaentrada[1];
  execute v_sql into v_maxcod;
  
  for i in 1..array_upper(p_tabelaentrada, 1) loop
      execute f_atualizagid(p_tabelaentrada[i]);
      execute 'select count(pon_codigo) from '||p_tabelaentrada[i] into v_numeropontostabelaentrada;
      v_numeroElementos := v_numeroElementos + v_numeropontostabelaentrada;
  end loop;

  v_diferenca :=  p_qtdepontos  - p_sensores;
  --execute 'select (('||v_maxcod||'!)/(('||p_sensores||'!) * ('||v_diferenca||'!)))' into v_limite;
  --v_combinacoes := ((v_maxcod !)/((p_sensores !) * (v_diferenca !)));
 -- v_limite := v_combinacoes;
  raise notice 'Número de Combinações: %', v_limite;

  v_sql := 'create table '||p_tabelasaida||' (pon_codigo text, centroid text)';
  execute v_sql;

  v_sql := 'select ''{''||';
  for i in 1..array_upper(p_tabelaentrada, 1) loop
      if (i = (array_upper(p_tabelaentrada, 1))) then
	v_sql := v_sql || 'a'||i||'.pix_valor '||'||''}'' as matriz';
      else
	v_sql := v_sql || 'a'||i||'.pix_valor '||'||'', ''||';
      end if;
  end loop;
  v_sql := v_sql || ' from ';
  for i in 1..array_upper(p_tabelaentrada, 1) loop
      if (i = (array_upper(p_tabelaentrada, 1))) then
	v_sql := v_sql || ' '|| p_tabelaentrada[i] ||' a'||i||' ';
      else
	v_sql := v_sql || ' '|| p_tabelaentrada[i] ||' a'||i||',';
      end if;
  end loop;
  v_sql:= v_sql|| ' where ';
  for i in 1..array_upper(p_tabelaentrada, 1) loop
      v_sql := v_sql ||' a'||i||'.pon_codigo = a1.pon_codigo and';
  end loop;
  v_sql:= v_sql|| ' a1.pon_codigo in ';

  execute 'SELECT CAST ( 0 AS numeric ) ' into v_numericzero;
  
  For i in 1..v_limite loop
       n:= 1;
       while n <= p_sensores loop
             execute 'SELECT round(CAST (random()*'||p_qtdepontos ||' AS NUMERIC),0)' into v_sorteado;
             if (v_sorteado = v_numericzero) then
		while (v_sorteado = v_numericzero) loop
			execute 'SELECT round(CAST (random()*'||p_qtdepontos||' AS NUMERIC),0)' into v_sorteado;
		end loop;
	     end if;
            -- select position(','||v_sorteado||',' in (','||cast(v_matriz as text)||',')) into posicao;
             if not ((position(','||v_sorteado||',' in (','||cast(v_matriz as text)||','))<> 0)  or
                (position('{'||v_sorteado||',' in ('{'||cast(v_matriz as text)||','))<> 0)  or
                (position(','||v_sorteado||'}' in (','||cast(v_matriz as text)||'}'))<> 0) ) then 
                v_matriz[n]:= v_sorteado;
            
              --  raise notice 'n: % - v_matriz[%]:% - posicao:%', n, n, v_matriz[n], posicao;
                n:=n+1;
             end if;
       end loop;    
       raise notice 'matriz: % ', v_matriz; 

      v_sql2 = '(';
      for i in 1..array_upper(v_matriz, 1) loop
	if (i = (array_upper(v_matriz, 1))) then
		v_sql2 := v_sql2||v_matriz[i]||')';
	else
		v_sql2 := v_sql2||v_matriz[i]||',';
	end if;
      end loop;

       v_sql3:= v_sql||v_sql2 ;
          raise notice 'SQL: %', v_sql3;
       open v_cursor for execute v_sql3;
       v_textomatriz := '{';
       LOOP FETCH v_cursor INTO v_dado;
EXIT WHEN NOT FOUND;
      v_textomatriz := v_textomatriz|| v_dado.matriz||',';
      --raise notice 'final: % ', v_textomatriz;
       END LOOP;
Close v_cursor;
v_textomatriz := v_textomatriz || '}$';
       execute 'select replace ('''||v_textomatriz||''', '',}$'', ''}'')' into v_textomatriz;
       raise notice 'final: % ', v_textomatriz;
       v_sql4 := 'insert into '||p_tabelasaida||'(pon_codigo, centroid) values ('''||v_sql2||''', '''||v_textomatriz||''')';
       --raise notice 'final: % ', v_sql;
       execute v_sql4;  
  end loop;
  v_retorno := v_limite;

  return   p_qtdepontos;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_inserecombinacao(integer, integer, integer, text[], text, integer)
  OWNER TO postgres;
