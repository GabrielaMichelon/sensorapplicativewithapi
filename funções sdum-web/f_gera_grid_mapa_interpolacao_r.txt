-- Function: public.f_gera_grid_mapa_interpolacao_r(integer, text, double precision, double precision, double precision[], double precision[], text, integer, integer)

-- DROP FUNCTION public.f_gera_grid_mapa_interpolacao_r(integer, text, double precision, double precision, double precision[], double precision[], text, integer, integer);

CREATE OR REPLACE FUNCTION public.f_gera_grid_mapa_interpolacao_r(
    p_codarea integer,
    p_nometabelaareas text,
    p_tam_x double precision,
    p_tam_y double precision,
    p_pontos double precision[],
    p_pixvalor double precision[],
    p_mapa text,
    p_codgrade integer,
    p_sriddefaultbanco integer)
  RETURNS boolean AS
$BODY$
DECLARE
	i	int:=1;
	j	int := 1;
  V_xmin  float;             -- x m�nimo do pol�gono
  V_xmax  float;             -- x m�ximo do poligono
  V_ymin  float;             -- y m�nimo do poligono
  V_ymax  float;             -- y m�ximo do poligono
  V_nPixel_x  integer;       -- n�mero de pixels de x
  V_nPixel_y  integer;       -- n�mero de pixels de y  
  V_largura   float;         -- largura total da �rea;
  V_altura    float;         -- altura total da �rea;
  V_tamPixel_x float;        -- tamanho do pixel x
  V_tamPixel_y float;        -- tamanho do pixel y
  V_x          integer := 0; -- usado para o LOOP
  V_y          integer := 0; -- usado para o LOOP
  Posicao_x    float;        -- necess�rio para quando o poligono inicia na posi��o (0,0)
  Posicao_y    float;        -- necess�rio para quando o poligono inicia na posi��o (0,0)
  V_Str_Insert text;         -- string para executar o insert
  V_str_Delete text;         -- string para executar o delete dos pontos que est�o fora do poligono
  V_str_DeleteST_POINT text; -- string para executar o delete dos pontos inseridos no banco na forma de ST_POINT, pois o R devolve ponto e n�o pol�gono
  local_srid    integer;   -- ALTERAR QUANDO FOR PRECISO ALTERAR A REGI�O
  v_longcentral float;
  v_latcentral float;
  v_recebelinhatabela text;
  auxiliar integer := array_length(p_pontos, 1);
BEGIN
      execute 'select st_x(st_centroid(the_geom)), st_y(st_centroid(the_geom)) from '||p_nometabelaareas|| '
               where id = '||p_codarea into v_longcentral, v_latcentral;
      select f_buscadatum_utm(v_longcentral, v_latcentral) into local_srid;
      raise notice 'srid:%', local_srid;
	  
    V_tamPixel_x := p_tam_x;     -- define o tamanho de cada ponto da horizontal
    V_tamPixel_y := p_tam_y;  

    --  EXECUTE 'CREATE TABLE '||P_Mapa||' (	gid serial primary key, 
	--					gri_medida float)';   -- cria a tabela onde o novo gride ser� armazenado
    -- EXECUTE ' SELECT AddGeometryColumn('''||P_Mapa||''', ''geometry'', '||P_SridDefaultBanco||', '''||P_Tipogeometria||''', 2)'; 
	raise notice 'array_upper %',auxiliar;	 
      while(i<auxiliar) loop
		V_str_Insert := 'insert into '||P_Mapa||'(the_geom, pix_valor, pix_mapcodigo) values '||
									  '(st_transform(st_geomfromtext(''POLYGON(('||p_pontos[i]-(V_TamPixel_x/2)|| ' '||p_pontos[i+1]-(V_TamPixel_y/2)||',
									   '||p_pontos[i]+(V_TamPixel_x/2)|| ' '||p_pontos[i+1]-(V_TamPixel_y/2)||',
									   '||p_pontos[i]+(V_TamPixel_x/2)|| ' '||p_pontos[i+1]+(V_TamPixel_y/2)||',
									   '||p_pontos[i]-(V_TamPixel_x/2)|| ' '||p_pontos[i+1]+(V_TamPixel_y/2)||',
									   '||p_pontos[i]-(V_TamPixel_x/2)|| ' '||p_pontos[i+1]-(V_TamPixel_y/2)||'))'', '||local_srid||'),'||P_SridDefaultBanco||'), '||p_pixvalor[j]||', '||p_codgrade||');';   
									   j:=j+1;
									   i := i+2;
									   
		raise notice 'i %',i;	  
		execute V_Str_Insert;          
		--raise notice 'consulta %',v_recebelinhatabela;
      END LOOP;
	  
	  --V_str_Delete := 'delete from '||P_Mapa||' b where b.pix_mapcodigo = '|| p_codgrade ||' and b.pix_codigo not in (select a.pix_codigo from '||P_Mapa||' a, '||p_nometabelaareas||' c
      --      where (st_Intersects( c.the_geom, a.the_geom) or st_contains( c.the_geom, a.the_geom) ) and c.id = '||P_Codarea||')';
      --execute V_str_Delete;

      --execute 'select f_gera_grid_mapa_interpolacao_r2('||p_codgrade||')';
	  
      return true;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_gera_grid_mapa_interpolacao_r(integer, text, double precision, double precision, double precision[], double precision[], text, integer, integer)
  OWNER TO "sdum-web";
