-- Function: public.f_interpolador_ponto_mm(integer, text, text, double precision, integer, double precision, text)

-- DROP FUNCTION public.f_interpolador_ponto_mm(integer, text, text, double precision, integer, double precision, text);

CREATE OR REPLACE FUNCTION public.f_interpolador_ponto_mm(
    p_interpolacao integer,
    p_refinterpolacao text,
    p_grade text,
    p_expoente double precision,
    p_elementos integer,
    p_raio double precision,
    p_coluna text)
  RETURNS double precision AS
$BODY$
DECLARE
    Cur_Amostras   	 refcursor;
    V_Pontos   		 GEOMETRY;
    V_Distancia 	 FLOAT;
    V_Medida    	 FLOAT;
    V_Soma      	 FLOAT:=0;
    V_Numerador		 FLOAT:=0;
    V_Denominador	 FLOAT:=0;
    V_Estimado		 FLOAT:=0;
    i 			 integer:=0;
    V_QUERY_SELECT 	 text;
BEGIN
		raise notice 'ENTROU NO INTERPOLADOR-MM**************************%--------------p_elementos: %', P_interpolacao,p_elementos;
	   V_QUERY_SELECT := 'SELECT  a.the_geom, st_distance(b.the_geom, a.the_geom) as distancia,  a.'||P_Coluna||' as medida
           FROM '||P_RefInterpolacao||' a, '||P_Grade||'  b
           WHERE b.pix_codigo = '||P_interpolacao;
           IF P_Elementos > 0 THEN
		V_QUERY_SELECT := V_QUERY_SELECT ||' order by distancia LIMIT '||P_Elementos ;
	   ELSE
	        V_QUERY_SELECT := V_QUERY_SELECT ||' and st_contains(st_buffer(st_centroid(b.the_geom), '||P_Raio||'), a.the_geom)';
           END IF;
	OPEN Cur_Amostras FOR EXECUTE   V_QUERY_SELECT;
	LOOP FETCH Cur_Amostras INTO V_Pontos, V_Distancia, V_Medida;
	  EXIT WHEN NOT FOUND;
	   raise notice 'V_MEDIDA**************************%',V_Medida;
           i:= i+1;
           V_Numerador := V_Numerador + V_Medida;
        END LOOP;
        IF i > 0 THEN
            V_Soma := V_Numerador/i;
        ELSE
           RETURN 0;
        END IF;
      CLOSE Cur_Amostras;
      RETURN V_Soma;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_interpolador_ponto_mm(integer, text, text, double precision, integer, double precision, text)
  OWNER TO postgres;
