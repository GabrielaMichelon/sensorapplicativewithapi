-- Function public.f_atualizagid(text)

-- DROP FUNCTION public.f_atualizagid(text);

CREATE OR REPLACE FUNCTION public.f_atualizagid(p_tabela text)
  RETURNS void AS
$BODY$
declare
  v_cursor refcursor;
  v_dado record;
  i integer =1;
  v_sql text;
begin
   open v_cursor for execute 'select  from '||p_tabela||' order by pon_codigo';

   LOOP FETCH v_cursor INTO v_dado;
EXIT WHEN NOT FOUND;
v_sql = 'UPDATE '||P_TABELA||' SET pon_codigo = '||i||'where pon_codigo = '||v_dado.pon_codigo;
execute v_sql;
i = i+1;
   END LOOP;
Close v_cursor;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.f_atualizagid(text)
  OWNER TO postgres;
